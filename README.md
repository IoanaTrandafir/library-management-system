# Configurare mediu de lucru


### Tools

1. Maven 3.0+ 
2. Un IDE (noi am folosit IntelliJ)
3. JDK 1.8+
4. PosttgreSQL

### Instalare JDK


Verificam daca java este deja instalat:

```
$ java -version
```

Daca rezultatul este negativ, instalam JRE

```
$ brew install openjdk@8
```
si verificam iar daca s-a instalat:

```
$ java -version
```

Daca vedem urmatorul output:

```
Output
openjdk version "1.8.0_162"
OpenJDK Runtime Environment (build 1.8.0_162-8u162-b12-1-b12)
OpenJDK 64-Bit Server VM (build 25.162-b12, mixed mode)
```
     
Continuam cu instalarea JDK-ului:

```
$ sudo apt install openjdk-8-jdk
```

Cele mai multe programe java utilizeaza variabila de mediu JAVA_HOME astfel incat trebuie setata. 


```
$ sudo update-alternatives --config java
```

Mai intai trebuie sa vedem unde este instalat. In tabel cautam /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java:

---
Deschidem fisierul /etc/environment 


```
$ sudo nano /etc/environment
```

La sfarsitul fisierului inlocuim calea cu cea determinata mai sus:

```
JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64/bin/"
```

Reincarcam fisierul:
```
$ source /etc/environment
```

Verificam daca modificarea a avut loc
```
$ echo $JAVA_HOME
```

Outputul trebuie sa coincida cu modificarea facuta mai sus.
```
Output
/usr/lib/jvm/java-11-openjdk-amd64/bin/
```

### Instalare Maven

```
$  brew install maven
```

``` 
$ mvn -version
```


### Instalare PosttgreSQL

```
$ brew update
$ brew install postgresql
```

Accesare consola psql:

```
$  sudo -u postgres psql postgres
```

Cream user pentru a accesa aplicatia:

```
postgres=#  create user USER with password;
```

Cream baza de date:

```
postgres=# create database librarydb;
```

Acordam drepturi userului creat anterior:
```
postgres=# grant all privileges on database librarydb to USER;
```

## Rulare aplicatie

Aplicatia este construita in SpringBoot si rulata cu ajutorul Maven.

Pentru a rula aplicatia, intai trebuie sa instalam dependintele

Intram in proiect, in /library-managemet-system si rulam 

```
$ mvn install
```

```
$ mvn spring-boot:run
```
